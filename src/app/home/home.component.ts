import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {NewTaskComponent} from '../new-task/new-task.component';
import {KanbanApiServiceService} from '../core/api-service/kanban-api-service.service';
import {kanbanTable} from '../model/model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  tables: Array<kanbanTable> = [];
  constructor(
    public dialog: MatDialog,
    private kanbanApiService: KanbanApiServiceService
  ) {
    this.kanbanApiService.getAllKanban().subscribe(
      (res: Array<kanbanTable>) => {this.tables = res; console.log(this.tables)});
  }

  ngOnInit(): void {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NewTaskComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
      console.log(`Dialog result: ${result}`);
    });
  }
}
