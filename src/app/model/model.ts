export interface kanbanTable  {
  id: number;
  cards: Array<kanbanCard>;

}
export class kanbanCard {
  id: number;
  sheet: kanbanSheet;
  position: number;
  positionInTable: number;
  state: string;
}
export class kanbanSheet{
  id: number;
  wording: string;
  dueDate: Date;
  user: user;
  timeNeeded: number;
  tags: Array<tag>;
  location: string;
  url: string;
  note: string;
}
export class user{
  id: number;
  firstName: string;
  lastName: string;
  kanbanSheet: kanbanSheet[];
}
export class developper extends user{

}
interface tag{
  id: number;
  tag: string;
}
