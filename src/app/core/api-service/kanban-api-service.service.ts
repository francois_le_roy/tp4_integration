import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {kanbanCard, user} from '../../model/model';

@Injectable({
  providedIn: 'root'
})
export class KanbanApiServiceService {

  constructor(
    private httpClient: HttpClient
  ) { }

  postNewKanban(kanban): Observable<any> {
    return this.httpClient.post('/api/table', kanban);
  }
  getKanbanById(id): Observable<any> {
    return this.httpClient.get('/api/table/' + id);
  }
  getAllKanban(): Observable<any>{
    return this.httpClient.get('/api/table');
  }

  postNewCard(actualId: number, newKanbanCard: kanbanCard): Observable<any> {
    return this.httpClient.post('/api/table/' + actualId + '/card', newKanbanCard);
  }

  createTable(): Observable<any> {
    // create empty kanban table
    return this.httpClient.post('/api/table', {
      cards: []
    });
  }

  changeCardState(tableID, cardID, state): Observable<any> {
    return this.httpClient.post('/api/table/' + tableID + '/card/' + cardID + '/' + state,{});
  }

  deleteCard(tableID: number, cardID: number): Observable<any> {
    return this.httpClient.delete('/api/table/' + tableID + '/card/' + cardID);
  }

  getAllUser(): Observable<any> {
    return this.httpClient.get('/api/user');
  }

  postUser(userToPost: user): Observable<any> {
    return this.httpClient.post('/api/user/', userToPost);

  }
}
