import { TestBed } from '@angular/core/testing';

import { KanbanApiServiceService } from './kanban-api-service.service';

describe('KanbanApiServiceService', () => {
  let service: KanbanApiServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KanbanApiServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
