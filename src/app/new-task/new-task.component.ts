import { Component, OnInit } from '@angular/core';
import {KanbanApiServiceService} from '../core/api-service/kanban-api-service.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {developper, kanbanCard, kanbanSheet, user} from '../model/model';
import {CreateUserComponent} from '../create-user/create-user.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css']
})
export class NewTaskComponent implements OnInit {
  newTaskForm = new FormGroup({
    wording: new FormControl(''),
    date: new FormControl(''),
    time: new FormControl(''),
    location: new FormControl(''),
    tags: new FormControl(''),
    url: new FormControl(''),
    note: new FormControl(''),
    selectedUserForm: new FormControl()
  });
  users: user[];
  constructor(
    private kanbanApiService: KanbanApiServiceService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog
) {

  }

  ngOnInit(): void {
    this.kanbanApiService.getAllUser().subscribe(
      (res: user[]) => { this.users = res; }
    );
  }

  onSubmit(data): void {
    console.log(data.selectedUserForm);
    const newKanbanSheet = new kanbanSheet();
    newKanbanSheet.wording = data.wording;
    newKanbanSheet.note = data.note;
    newKanbanSheet.dueDate = data.date;
    newKanbanSheet.location = data.location;
    const tags = [];
    tags.push(data.tags);
    newKanbanSheet.tags = tags;
    newKanbanSheet.timeNeeded = data.time;
    newKanbanSheet.url = data.url;
    newKanbanSheet.user = data.selectedUserForm;
    const newKanbanCard = new kanbanCard();
    newKanbanCard.sheet = newKanbanSheet;
    newKanbanCard.positionInTable = 0;
    newKanbanCard.position = 0;
    this.kanbanApiService.postNewCard(1, newKanbanCard).subscribe(
      res => {
        console.log(res);
        window.location.reload();
      }
  );
  }

  createUser(event): void {
      event.preventDefault();
      const dialogRef = this.dialog.open(CreateUserComponent, {
      });
      dialogRef.afterClosed().subscribe(results => {
      this.kanbanApiService.getAllUser().subscribe(
        (res: user[]) => { this.users = res; }
      );
    });
  }
}
