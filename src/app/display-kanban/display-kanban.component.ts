import { Component, OnInit } from '@angular/core';
import {kanbanTable} from '../model/model';
import {KanbanApiServiceService} from '../core/api-service/kanban-api-service.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-display-kanban',
  templateUrl: './display-kanban.component.html',
  styleUrls: ['./display-kanban.component.css']
})
export class DisplayKanbanComponent implements OnInit {
  tables: Array<kanbanTable>;
  selectedTable: kanbanTable;
  id = 1;
  constructor(
    private kanbanApiService: KanbanApiServiceService
  ) {

  }
  changeSelected(id): void{
    this.id = id;
  }
  ngOnInit(): void {
    this.kanbanApiService.getAllKanban().subscribe(
      (res: Array<kanbanTable>) => {this.tables = res; });
    this.kanbanApiService.getKanbanById(this.id).subscribe(
      (res: kanbanTable) => {this.selectedTable = res; });
  }

  drop(event: CdkDragDrop<string[]>): void {
    const selectedCardId = event.item.element.nativeElement.id;
    console.log(event.item.element.nativeElement.children);
    const droppedArea = event.container.element.nativeElement.classList;
    if (droppedArea.contains('waitings')){
      this.kanbanApiService.changeCardState(this.id, selectedCardId, 'Waiting').subscribe(
        res => console.log(res)
      );
    }
    if (droppedArea.contains('inProgress')){
      this.kanbanApiService.changeCardState(this.id, selectedCardId, 'Progress').subscribe(
        res => console.log(res)
      );
    }
    if (droppedArea.contains('finished')){
      console.log(droppedArea.contains('finished'));
      this.kanbanApiService.changeCardState(this.id, selectedCardId, 'Finished').subscribe(
        res => console.log(res)
      );
    }
    this.kanbanApiService.getKanbanById(this.id).subscribe(
      (res: kanbanTable) => {this.selectedTable = res; });
    this.ngOnInit();
  }

  deleteCard(id: number): void {
    this.kanbanApiService.deleteCard(this.id, id).subscribe();
    this.ngOnInit();
  }
}
