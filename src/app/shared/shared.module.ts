import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TopbarModule} from './topbar/topbar.module';
import {MaterialModule} from '../material/material.module';
import { KanbanStatePipe } from './kanban-state.pipe';



@NgModule({
  declarations: [KanbanStatePipe],
  imports: [
    CommonModule,
    TopbarModule,
    MaterialModule,
  ],
  exports : [
    TopbarModule,
    KanbanStatePipe
  ]
})
export class SharedModule { }
