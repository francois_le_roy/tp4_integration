import { Pipe, PipeTransform } from '@angular/core';
import {kanbanCard} from '../model/model';

@Pipe({
  name: 'kanbanState'
})
export class KanbanStatePipe implements PipeTransform {

  transform(value: Array<kanbanCard>, args: string): Array<kanbanCard> {
    return value.filter(card => card.state === args);
  }
}
