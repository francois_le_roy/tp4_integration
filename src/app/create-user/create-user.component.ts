import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {KanbanApiServiceService} from '../core/api-service/kanban-api-service.service';
import {user} from '../model/model';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  newUserForm = new FormGroup(
    {
      firstname : new FormControl(''),
      lastname: new FormControl('')
    }
  );

  constructor(
    private kanbanApiService: KanbanApiServiceService
  ) {

  }

  ngOnInit(): void {
  }

  onSubmit(value: any): void {
    console.log(value);
    const userToPost = new user();
    userToPost.firstName = value.firstname;
    userToPost.lastName = value.lastname;
    userToPost.kanbanSheet = [];
    console.log(userToPost);
    this.kanbanApiService.postUser(userToPost).subscribe();
  }
}
