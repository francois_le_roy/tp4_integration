import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoTableComponent } from './no-table.component';

describe('NoTableComponent', () => {
  let component: NoTableComponent;
  let fixture: ComponentFixture<NoTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
