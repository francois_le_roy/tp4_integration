import { Component, OnInit } from '@angular/core';
import {KanbanApiServiceService} from '../core/api-service/kanban-api-service.service';

@Component({
  selector: 'app-no-table',
  templateUrl: './no-table.component.html',
  styleUrls: ['./no-table.component.css']
})
export class NoTableComponent implements OnInit {

  constructor(
    private kanbanApiService: KanbanApiServiceService
  ) { }

  ngOnInit(): void {
  }

  tableCreate(): void {
    this.kanbanApiService.getAllKanban().subscribe(
      res => {
        if (res.length === 0){
          // if there is no kanban table we insert an empty one
          this.kanbanApiService.createTable().subscribe(
            () => window.location.reload()
        );
        }
      }
    );
  }
}
